export class Meaning {
   dictionary={
    address:'Dirección',
    city:'Ciudad',
    country:'Pais',
    email:'Correo',
    name:'Nombre',
    password:'Contraseña',
    phone:'Telefono',
    rol:'Rol',
    userRol:'Rol Usuario',
    state:'Estado',
    id:'id'
   }
}