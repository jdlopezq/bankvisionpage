import { Component, OnInit, ViewChild, Input } from "@angular/core";
import { MatToolbar } from "../../../../node_modules/@angular/material";
import { NgsRevealConfig } from "ng-scrollreveal";

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.css"]
})
export class NavbarComponent implements OnInit {
  @ViewChild("navbar") navBar: MatToolbar;
  toolBarColor = true;
  footerSize = true;
  buttonTop = false;
  divSize;
  index;
  constructor(config: NgsRevealConfig) {
    this.divSize = window.innerHeight;
    config.scale = 1;
    config.origin = "left";
    config.reset = true;
  }

  ngOnInit() {
    console.log(window.innerWidth);

    this.divSize = window.innerHeight;
    window.onscroll = () => {
      if (window.scrollY === 0) {
        this.toolBarColor = true;
        this.footerSize = true;
        this.buttonTop = false;
      } else {
        this.toolBarColor = false;
        this.footerSize = false;
        this.buttonTop = true;
      }
    };
  }

  resumeFooter() {
    if (window.scrollY !== 0) {
      this.footerSize = false;
      this.buttonTop = true;
    }
  }

  fullFooter() {

    
    this.footerSize = true;
    this.buttonTop = false;
  }

  goTop() {
    window.scroll({ behavior: "smooth", top: 0 });
  }
  goProducts(index) {
    this.index = index;
    if (window.innerWidth > 740) {
      document
        .getElementById("products")
        .scrollIntoView({ behavior: "smooth" });
    } else {
      document.getElementById("products").scrollIntoView();
    }
  }

  goInfo() {
    if (window.innerWidth > 740) {
      document
        .getElementById("information")
        .scrollIntoView({ behavior: "smooth" });
    } else {
      document
        .getElementById("information")
        .scrollIntoView({ behavior: "smooth" });
    }
  }

  gofindUs() {
    if (window.innerWidth > 740) {
      document.getElementById("findus").scrollIntoView({ behavior: "smooth" });
    } else {
      document.getElementById("findus").scrollIntoView();
    }
  }
  goApps() {
    if (window.innerWidth > 740) {
      document
        .getElementById("apps")
        .scrollIntoView({ behavior: "smooth" });
    } else {
      document.getElementById("costumers").scrollIntoView();
    }
  }
  goCostumers() {
    if (window.innerWidth > 740) {
      document
        .getElementById("costumers")
        .scrollIntoView({ behavior: "smooth" });
    } else {
      document.getElementById("costumers").scrollIntoView();
    }
  }
  
  goCotactUs() {
    if (window.innerWidth > 740) {
      document.getElementById("contactus").scrollIntoView({ behavior: "smooth" });
    } else {
      document.getElementById("contactus").scrollIntoView({ behavior: "smooth" });
    }
  }
  goLanding() {
    if (window.innerWidth > 740) {
      document.getElementById("landing").scrollIntoView({ behavior: "smooth" });
    } else {
      document.getElementById("landing").scrollIntoView({ behavior: "smooth" });
    }
  }
}
