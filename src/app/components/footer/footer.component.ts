import { Component, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { MatTooltip } from '../../../../node_modules/@angular/material';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

tipBogota
tipMedellin
tipMiami
  constructor() { }

  ngOnInit() {
    this.tipBogota=`Bogotá D.C. - Colombia
    Carrera 7 bis 124-26
    Edificio AMC Of. 601
    PBX: (571) 7421982`
    this.tipMedellin=`Medellín - Colombia
    Carrera 43a # 1 sur -192
    Torre Davivienda Of. 510
    PBX: (574) 2688277`
    this.tipMiami=`Tampa - USA
    2736 Land O'lakes Blvd.
    Land O'Lakes Blvd 34639
    PBX: (813) 3730335`
  }

}
