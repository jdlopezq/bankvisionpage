import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-smartvisiondetails",
  templateUrl: "./smartvision.component.html",
  styleUrls: ["./productdetails.component.css"]
})
export class Smartcomponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}

@Component({
  selector: "app-coredetails",
  templateUrl: "./core.component.html",
  styleUrls: ["./productdetails.component.css"]
})
export class Corecomponent implements OnInit {
  constructor() {}
  showDetails = false;
  details1 = [
    "Sistema de información de clientes",
    "Proceso de crédito",
    "Vinculación de clientes",
    "Depósitos a la vista",
    "Remesas",
    "Depósitos a término",
    "Préstamos",
    "Cobranza",
    "Garantías y provisiones",
    "Cupos",
    "Transferencias automáticas",
    "Contabilidad",
    "Información Tributaria",
    "Autorizador de transacciones en línea"
  ];
  details2 = [
    "Cheques de gerencia",
    "Información tributaria",
    "Autorizador de transacciones en línea",
    "Cheques de gerencia",
    "Préstamos a constructores",
    "Administración de castigos",
    "Cuentas",
    "Sensitivas",
    "Conciliación",
    "Auxiliar contable",
    "Sistemas cruzados",
    "Alertas",
    "Caja (Oficinas)",
    "Administración y evaluación de cartera (endeudamiento)"
  ];

  ngOnInit() {}

  showInfo() {
    this.showDetails = !this.showDetails;
  }
}

@Component({
  selector: "app-botdetails",
  templateUrl: "./bot.component.html",
  styleUrls: ["./productdetails.component.css"]
})
export class Botcomponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}

@Component({
  selector: "app-factorydetails",
  templateUrl: "./factory.component.html",
  styleUrls: ["./productdetails.component.css"]
})
export class Factorycomponent implements OnInit {
  showDetails;
  info = [
    "Simulador de créditos",
    "Consulta en centrales de riesgo - Scoring",
    "Formularios electrónicos base",
    "BankVision",
    "Cálculos BankVision",
    "Asesoría especializada"
  ];

  constructor() {}

  ngOnInit() {
    this.showDetails = true;
  }

  showMore(){
    this.showDetails=! this.showDetails
  }
}

@Component({
  selector: "app-officedetails",
  templateUrl: "./office.component.html",
  styleUrls: ["./productdetails.component.css"]
})
export class Officecomponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
@Component({
  selector: "app-chargedetails",
  templateUrl: "./charge.component.html",
  styleUrls: ["./productdetails.component.css"]
})
export class Chargecomponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}

@Component({
  selector: "app-portaldetails",
  templateUrl: "./portal.component.html",
  styleUrls: ["./productdetails.component.css"]
})
export class Portlacomponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}

@Component({
  selector: "app-vinculationdetails",
  templateUrl: "./vinculation.component.html",
  styleUrls: ["./productdetails.component.css"]
})
export class Vinculationcomponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
