import { Component, OnInit } from '@angular/core';
import { Router } from '../../../../node_modules/@angular/router';


@Component({
  selector: 'app-costumers',
  templateUrl: './costumers.component.html',
  styleUrls: ['./costumers.component.css']
})
export class CostumersComponent implements OnInit {
clients=[{name:"Bancolombia", image:"bancolombia.png", h:"100%", w:"100%", r:"50%", m:"0px", url:"https://www.grupobancolombia.com"},
{name:"jurscoop", image:"juriscoop.png", h:"75%", w:"75%", r:"0", m:"10%", url:"https://www.financierajuriscoop.com.co/"},
{name:"Coofinep", image:"coofinep.png", h:"35%", w:"100%", r:"0", m:"33%", url:"https://www.coofinep.com/"},
{name:"Reditos", image:"reditos.png", h:"100%", w:"100%", r:"50%", m:"0px", url:"http://www.gruporeditos.com/"},
{name:"Colfuturo", image:"colfuturo.png", h:"25%", w:"96%", r:"0", m:"30%", url:"https://www.colfuturo.org/"},
{name:"Sodeimpro", image:"sodeimpro.png", h:"90%", w:"90%", r:"50%", m:"0px", url:""},
{name:"Alianza", image:"alianza.png", h:"45%", w:"90%", r:"0", m:"22%", url:"http://www.alianza.com.co/"},
{name:"Movii", image:"movii.gif", h:"90%", w:"90%", r:"50%", m:"4%", url:"http://www.movii.com.co/"},
{name:"Scare", image:"scare.png", h:"90%", w:"90%", r:"50%", m:"4%", url:"https://www.scare.org.co/"},
{name:"Coink", image:"coink.png", h:"35%", w:"90%", r:"0", m:"33%", url:"http://coink.com/"},
{name:"Colciencias", image:"colciencias.png", h:"35%", w:"90%", r:"0", m:"33%", url:"http://www.colciencias.gov.co/"},


]
isHandset
  constructor() { }

  ngOnInit() {
if (window.innerWidth > 740) {
  this.isHandset=false
} else {
  this.isHandset=true
  
}
  }

  goTo(a){
console.log(a)
  }

}
