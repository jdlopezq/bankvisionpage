import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import {
  SwiperComponent,
  SwiperDirective,
  SwiperConfigInterface,
} from 'ngx-swiper-wrapper';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})

export class ProductsComponent implements OnInit {
  @Input() public index = 0;
  @Output() public indexOut: EventEmitter<number>;

  public slides = [
    'Core',
    'SmartVision',
    'Solución de Oficina',
    'Fábrica de creditos',
    'Portal Transaccional',
    'Vinculaciones',
    'Cobranzas',
    'ChatBot'
  ];

  public config: SwiperConfigInterface = {
    direction: 'horizontal',
    autoplay: true,
    speed: 500,
    effect: 'coverflow',
    slidesPerView: 1,
    keyboard: true,
    mousewheel: false,
    scrollbar: false,
    navigation: true,
    pagination: true,
    loop:true
  };

  @ViewChild(SwiperComponent) componentRef: SwiperComponent;
  @ViewChild(SwiperDirective) directiveRef: SwiperDirective;

  constructor() {
    this.indexOut = new EventEmitter();
  }

  ngOnInit() {}

  public onIndexChange(index: number) {
    this.indexOut.emit(index);
    // console.log('Swiper index: ', index);
  }
}
