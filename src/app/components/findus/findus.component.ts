import { Component, OnInit, AfterContentInit } from '@angular/core';
import { deflateRaw } from 'zlib';
declare var $: any;
@Component({
  selector: 'app-findus',
  templateUrl: './findus.component.html',
  styleUrls: ['./findus.component.css']
})
export class FindusComponent implements OnInit, AfterContentInit {
  constructor() {}
  bogotaLat = 4.699906;
  bogotaLon = -74.029709;
  medellinLat = 6.20165;
  medellinLon = -75.572214;
  miamiLat = 28.197986;
  miamiLong = -82.464312;
  showTampa = true;
  showMedellin = true;
  showBogota = true;

  ngAfterContentInit(): void {
    this.changeMap('mapBogota');
    this.changeMap('mapMedellin');
    this.changeMap('mapTampa');
    $(function() {
      /*
      var hour = new Date();
      var theme = hour.getHours() > 12 ? 'night' : 'day';
      $("#cardBogota").addClass('big-card');
      let mapBogota = mapfit.MapView('mapBogota', { theme: theme });
      // bogota map config
      mapBogota.setCenter([4.699906, -74.029709]);
      mapBogota.addMarker(mapfit.Marker([4.699906, -74.029709]));
      mapBogota.setZoom(15);

      // medellin map config

      let mapMedellin = mapfit.MapView('mapMedellin', { theme: theme });
      mapMedellin.setCenter([6.20165, -75.572214]);
      mapMedellin.addMarker(mapfit.Marker([6.20165, -75.572214]));
      mapMedellin.setZoom(15);

      //Tampa map config

      let mapTampa = mapfit.MapView('mapTampa', { theme: theme });
      mapTampa.setCenter([28.197986, -82.464312]);
      mapTampa.addMarker(mapfit.Marker([28.197986, -82.464312]));
      /*
      mapTampa.on('mouseover', function(e) {
        console.log('marker mouse over ');
      });
      
      mapTampa.setZoom(15);
      
      $('.mapfit-layer > canvas').each(function() {
        $(this).attr('id', 'prueba');
        $(this).attr('width', 1200);
        $(this).css('color' , 'red');
        console.log($(this)[0]);
        // $(this)[0].parent.width
        // $(this)[0].width = 1200;
        
      });
      */
    });
  }

  ngOnInit() {
    if (window.innerWidth > 740) {
      this.showMedellin = true;
      this.showBogota = true;
      this.showTampa = true;
    } else {
      this.showMedellin = false;
      this.showBogota = false;
      this.showTampa = false;
    }
  }
  showTamp() {
    this.showTampa = !this.showTampa;
  }
  showBog() {
    this.showBogota = !this.showBogota;
  }
  showMed() {
    this.showMedellin = !this.showMedellin;
  }

  changeMap(div) {
    $('#card' + div).addClass('big-card');
    $('#card' + div).append(`<div id="${div}" style="height: 75%;"><div>`);
    var hour = new Date();
    var theme = ((hour.getHours() >18 && hour.getHours() < 24) ||(hour.getHours() >=0 && hour.getHours() <6)) ? 'night' : 'day';
    setTimeout(() => {
      let mapBogota = mapfit.MapView(div, { theme: theme });
      mapBogota.setCenter([4.699906, -74.029709]);
      mapBogota.addMarker(mapfit.Marker([4.699906, -74.029709]));
      mapBogota.setZoom(15);
      $('#card' + div).removeClass('big-card'  );
    }, 100);

  }
}
