import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MaterialModule } from './shared/material.module';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NgsRevealModule } from 'ng-scrollreveal';
import 'hammerjs';
import { LandingComponent } from './components/landing/landing.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { AgmCoreModule } from '@agm/core';
import { FindusComponent } from './components/findus/findus.component';
import { ProductsComponent } from './components/products/products.component';

import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { FlexLayoutModule} from '@angular/flex-layout';
import { CostumersComponent } from './components/costumers/costumers.component';
import { AboutusComponent } from './components/aboutus/aboutus.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { Smartcomponent, Corecomponent, Botcomponent, Factorycomponent, Officecomponent, Chargecomponent, Portlacomponent, Vinculationcomponent} from './components/productdetails/productdetails.component';
import { AppsComponent } from './components/apps/apps.component';
import { ContactusComponent } from './components/contactus/contactus.component';




const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 'auto'
};

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LandingComponent,
    HeaderComponent,
    FooterComponent,
    FindusComponent,
    ProductsComponent,
    CostumersComponent,
    AboutusComponent,
    Smartcomponent, 
    Corecomponent,
    Botcomponent,
    Factorycomponent,
    Officecomponent,
    Chargecomponent,
    Portlacomponent,
    Vinculationcomponent,
    AppsComponent,
    ContactusComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    NgsRevealModule.forRoot(),
  
    SwiperModule,
    FlexLayoutModule,
    AngularFontAwesomeModule
  ],
  providers: [
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
